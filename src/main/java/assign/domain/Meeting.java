package assign.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@Entity //instance of a class in a raw in a table  // relation db // this annotation allows hibernate that this class is special, a db table type of special
@Table( name = "Meetings" )
@XmlRootElement(name = "meeting")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meeting {

	Long meetingId;
	
	String name;
	String year;
	Project projectId;
	
	public Meeting() {
		
	}
	
	public Meeting(String meeting, String year) {
		this.name = meeting;
		this.year = year;
	}
	
	public Meeting(String meeting, String year, Long meetingId) {
		this.name = meeting;
		this.year = year;
		this.meetingId = meetingId;	
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getMeetingId() {
		return meetingId;
	}
	
	private void setMeetingId(Long meetingId) {
		this.meetingId = meetingId;
	}
	
	@ManyToOne
	@JoinColumn(name="projectId") //represents foreign key //
	public Project getProject() {
		return this.projectId;
	}
	
	public void setProject(Project p) {
		this.projectId = p;
	}
	
	@Column(name="Name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="Year")
	public String getYear() {
		return year;
	}
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	public void setYear(String year) {
		this.year = year;
	}
	
}



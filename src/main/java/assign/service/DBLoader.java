package assign.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import assign.domain.Assignment;
import assign.domain.Meeting;
import assign.domain.Project;
import assign.domain.UTCourse;

import java.util.logging.*;

public class DBLoader {
	private SessionFactory sessionFactory;
	
	Logger logger;
	
	public DBLoader() {
		// A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
        
        logger = Logger.getLogger("EavesdropReader");
	}
	
	public void loadData(Map<String, List<String>> data) {
		logger.info("Inside loadData.");
	}
	
	public Long addAssignment(String title) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long assignmentId = null;
		try {
			tx = session.beginTransaction();
			Assignment newAssignment = new Assignment(title, new Date(), new Long(1)); 
			session.save(newAssignment);
		    assignmentId = newAssignment.getId();
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return assignmentId;
	}
	
	public Long addAssignmentAndCourse(String title, String courseTitle) throws Exception {
		Session session = sessionFactory.openSession();		//open connection
		Transaction tx = null;								
		Long assignmentId = null;
		try {
			tx = session.beginTransaction(); //anything we do within transaction will be put together per transaction (atomitaccly)
			Assignment newAssignment = new Assignment( title, new Date() ); //creating assign
			UTCourse course = new UTCourse(courseTitle); //creating utcourse
			newAssignment.setCourse(course); //estabilshing relationship
			session.save(course);			//saving objects into session //course needs to be done first in order to be able to get foreign key //there's a INSERT statement behind it 
			session.save(newAssignment);	//saving objects //we HAVE TO DO IT IN THIS ORDER DUE TO THE KEYS (assign has a forerign key to the course table)
		    assignmentId = newAssignment.getId(); //if obj was persistent we wouldnt be able to do this
		    tx.commit(); 					//before this, all of this saved in memory  // foce insert
		} catch (Exception e) {		//when somethimg goes wrong we will rollback so there is no effect on the tables
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return assignmentId;
	}
	
	public Long addAssignmentsToCourse(List<String> assignments, String courseTitle) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long courseId = null;
		try {
			tx = session.beginTransaction();
			UTCourse course = new UTCourse(courseTitle);
			session.save(course);
			courseId = course.getId();
			for(String a : assignments) {
				Assignment newAssignment = new Assignment( a, new Date() );
				newAssignment.setCourse(course);
				session.save(newAssignment);
			}
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return courseId;
	}
	
	public List<Assignment> getAssignmentsForACourse(Long courseId) throws Exception {
		Session session = sessionFactory.openSession();		
		session.beginTransaction();
		String query = "from Assignment where course=" + courseId; // BAD PRACTICE
		List<Assignment> assignments = session.createQuery(query).list();		
		return assignments;
	}
	
	public List<Object[]> getAssignmentsForACourse(String courseName) throws Exception {
		Session session = sessionFactory.openSession();		
		session.beginTransaction();
		String query = "from Assignment a join a.course c where c.courseName = :cname";		
				//assignment -> represented as a temo varaible to refere to this table//
		List<Object[]> assignments = session.createQuery(query).setParameter("cname", courseName).list();
		
		return assignments;
	}
	
	public Assignment getAssignment(String title) throws Exception {
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Assignment.class).
        		add(Restrictions.eq("title", title));
		
		List<Assignment> assignments = criteria.list();
		
		if (assignments.size() > 0) {
			return assignments.get(0);			
		} else {
			return null;
		}
	}
	
	public UTCourse getCourse(String courseName) throws Exception {
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(UTCourse.class).
        		add(Restrictions.eq("courseName", courseName));
		
		List<UTCourse> courses = criteria.list();
		
		if (courses.size() > 0) {
			session.close();
			return courses.get(0);	
		} else {
			session.close();
			return null;
		}
	}
	
	public void deleteAssignment(String title) throws Exception {
		
		Session session = sessionFactory.openSession();		
		session.beginTransaction();
		String query = "from Assignment a where a.title = :title";		
				
		Assignment a = (Assignment)session.createQuery(query).setParameter("title", title).list().get(0);
		
        session.delete(a);

        session.getTransaction().commit();
        session.close();		
	}
	
	public void deleteCourse(String courseName) throws Exception {
		//query first
		Session session = sessionFactory.openSession();		
		session.beginTransaction();
		String query = "from UTCourse c where c.courseName = :courseName";		
				
		UTCourse c = (UTCourse)session.createQuery(query).setParameter("courseName", courseName).list().get(0);
		//then delete
        session.delete(c);

        session.getTransaction().commit();
        session.close();		
	}
	
	public Assignment getAssignment(Long assignmentId) throws Exception {
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Assignment.class).
        		add(Restrictions.eq("id", assignmentId));
		
		List<Assignment> assignments = criteria.list();
		
		return assignments.get(0);		
	}
		
	public Project getProject(String name) throws Exception {
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Project.class).
        		add(Restrictions.eq("name", name));
		
		List<Project> Project = criteria.list();
		
		if (Project.size() > 0) {
			session.close();
			return Project.get(0);	
		} else {
			session.close();
			return null;
		}
	}
	
	public Project getProject(Long Id) throws Exception {
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		System.out.println("ProjectId: " + Id);
//		String projectId = projectId.toString();		
		Criteria criteria = session.createCriteria(Project.class).
        		add(Restrictions.eq("projectId", Id));
		
		System.out.println("Criteria: " + criteria);
		
		List<Project> project = criteria.list();
		
		System.out.println("DBLoader: getProject");
		
		if (project.size() > 0) {
			session.close();
			return (Project) project.get(0);	
		} else {
			session.close();
			return null;
		}
	}
	
	public Long addProject(String project, String description) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long projectId = null;
		try {
			tx = session.beginTransaction();
			Project newProject = new Project(project, description); 
			session.save(newProject); //.delete 
		    projectId = newProject.getId();
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return projectId;
	}
	
	public Long addMeeting(String meeting, String year){
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long projectId = null;
		try {
			tx = session.beginTransaction();
			Meeting newMeeting = new Meeting(meeting, year); 
			session.save(newMeeting); //.delete 
		    projectId = newMeeting.getMeetingId();
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return projectId;
	}
	
	public void deleteProject(Long projectId) throws Exception {
		System.out.println("Inside DBLoader:  deleteProject");
		Session session = sessionFactory.openSession();		
		session.beginTransaction();
		String query2 = "from Projects p where p.projectId = :projectId";	
		String query = "DELETE FROM Projects p WHERE p.projectId = :projectId";
				
		Project p = (Project)session.createQuery(query).setParameter("projectId", projectId).list().get(0);
		
        session.delete(p);

        session.getTransaction().commit();
        session.close();
	}
	

	
	public Meeting getMeeting(Long meetingId) throws Exception {
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Meeting.class).
        		add(Restrictions.eq("meetingId", meetingId));
		
		List<Meeting> Meeting = criteria.list();
		
		if (Meeting.size() > 0) {
			return Meeting.get(0);			
		} else {
			return null;
		}
	}
	
	public Meeting updateMeeting(Long meetingId) throws Exception {
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Meeting.class).
				add(Restrictions.eq("meetingId", meetingId));
		
		List<Meeting> Meeting = criteria.list();
		
		if (Meeting.size() > 0) {
			return Meeting.get(0);			
		} else {
			return null;
		}
		
	}

}

//
////not required
////public Meeting updateProject(Long projectId, String projectName) throws Exception {}
//
//public List<Object[]> getMeetingForAProject(String projectName) throws Exception {
//	Session session = sessionFactory.openSession();		
//	session.beginTransaction();
//	String query = "from Meeting m join m.project m where m.projectName = :mname";		
//			
//	List<Object[]> Meeting = session.createQuery(query).setParameter("mname", projectName).list();
//	
//	return Meeting;
//}
//
//public List<Meeting> getMeetingForAProject(Long projectId) throws Exception {
//	Session session = sessionFactory.openSession();		
//	session.beginTransaction();
//	String query = "from Meeting where project= :projectId";// + projectId; // BAD PRACTICE
//	List<Meeting> Meeting = session.createQuery(query).setParameter("projectId", projectId).list();		
//	return Meeting;
//}
//

//
//public Long addMeetingAndProject(String meetingName, String projectName, String description, Long year) throws Exception {
//	Session session = sessionFactory.openSession();		//open connection
//	Transaction tx = null;								
//	Long projectId = null;
//	try {
//		tx = session.beginTransaction(); //anything we do within transaction will be put together per transaction (atomitaccly)
//		Meeting newMeeting = new Meeting(meetingName, year ); //creating assign
//		Project project = new Project(projectName, description); //creating utcourse
//		newMeeting.setProject(project);  //estabilshing relationship
//		session.save(project);			//saving objects into session //course needs to be done first in order to be able to get foreign key //there's a INSERT statement behind it 
//		session.save(newMeeting);	//saving objects //we HAVE TO DO IT IN THIS ORDER DUE TO THE KEYS (assign has a forerign key to the course table)
//	    projectId = newMeeting.getMeetingId(); //if obj was persistent we wouldnt be able to do this
//	    tx.commit(); 					//before this, all of this saved in memory  // foce insert
//	} catch (Exception e) {		//when something goes wrong we will rollback so there is no effect on the tables
//		if (tx != null) {
//			tx.rollback();
//			throw e;
//		}
//	}
//	finally {
//		session.close();			
//	}
//	return projectId;
//}
//

//public Long addMeetingToProject (List<String> Meeting, String meeting, String year) throws Exception {
//	Session session = sessionFactory.openSession();
//	Transaction tx = null;
//	Long projectId = null;
//	try {
//		tx = session.beginTransaction();
//		Project project = new Project(projectName, description); //name and description
//		session.save(project);
//		projectId = project.getId();
//		for(String a : Meeting) {
//			Meeting newMeeting = new Meeting(a, year); // name and year
//			newMeeting.setProject(project);
//			session.save(newMeeting);
//		}
//	    tx.commit();
//	} catch (Exception e) {
//		if (tx != null) {
//			tx.rollback();
//			throw e;
//		}
//	}
//	finally {
//		session.close();			
//	}
//	return projectId;
//}
//
//
//

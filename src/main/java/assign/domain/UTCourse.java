package assign.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table( name = "ut_courses" )
public class UTCourse {
	
	private Long id;  //column 
    private String courseName; //column
    private Set<Assignment> assignments; //a set of assignments //parent shouldnt know about children
    									//no column, a filed in the data class
    									//internat presentation that this parent is mapped by other child rolls
    //

    public UTCourse() {
    	// this form used by Hibernate
    }
    
    public UTCourse(String courseName) {
    	this.courseName = courseName;
    }
    
    @Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
		return id;
    }

    private void setId(Long id) {
		this.id = id;
    }
    
    @Column(name="course")
    public String getCourseName() {
		return courseName;
    }

    public void setCourseName(String courseName) {
		this.courseName = courseName;
    }
    
    //hibernate will be able to keep mappoing of child
    //parent to children
    @OneToMany(mappedBy="course") // name of the property where is should be mathcing from (this exists in the other class) public UTCourse @JoinColumn. KEEP CONSISTANCY 
    @Cascade({CascadeType.DELETE}) // from the parent to all the children in a single stroke. all rows in the child table (the relationship) will be deleted
    public Set<Assignment> getAssignments() {
    	return this.assignments;
    }
    
    public void setAssignments(Set<Assignment> assignments) {
    	this.assignments = assignments;
    }
}
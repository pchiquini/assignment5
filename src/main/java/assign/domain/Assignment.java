package assign.domain;

import java.util.Date;

//jpa annotations which will be used for all the other providers 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//hibernate annotation, this ties your program to the hibernate provider
import org.hibernate.annotations.GenericGenerator;


@Entity //instance of a class in a raw in a table  // relation db // this annotation allows hibernate that this class is special, a db table type of special
@Table( name = "assignments" )
public class Assignment {
	
	//any attributes for the table will be defined here, and the type for that atttribute
	private Long id;

    private String title;
    private Date date;  //use string for the project
    private UTCourse utcourse; // course or something else
    
    public Assignment() {
    	// this form used by Hibernate
    }
    
    public Assignment(String title, Date date) {
    	// for application use, to create new assignment
    	this.title = title;
    	this.date = date;
    }
    
    public Assignment(String title, Date date, Long providedId) {
    	// for application use, to create new assignment
    	this.title = title;
    	this.date = date;
    	this.id = providedId;
    }    
    
    @Id //this annotation defines how it will access the properties of the class. // this will directly manipulating field, but in this case hbernate ues persistnace state (GETTER/SETTER)
	//@GeneratedValue(generator="increment")    
	//@GenericGenerator(name="increment", strategy = "increment")
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
		return id;
    }

    private void setId(Long id) {
		this.id = id;
    }
    
    //for every column that gets created you can control what name it takes
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASSIGNMENT_DATE") //assigning the name, otherwise it would just take the name of the property
    public Date getDate() {
		return date;
    }

    public void setDate(Date date) {
		this.date = date;
    }
    
    //ASSIGNMENT5 representing foreign key  this is owning side
    @ManyToOne //annotation to be use as a foreign key reference
    @JoinColumn(name="course_id")
    public UTCourse getCourse() { // property named course available on this object
    	return this.utcourse;
    }
    
    public void setCourse(UTCourse c) {
    	this.utcourse = c;
    }

    public String getTitle() {
		return title;
    }

    public void setTitle(String title) {
		this.title = title;
    }
}

package assign.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity //instance of a class in a raw in a table  // relation db // this annotation allows hibernate that this class is special, a db table type of special
@Table( name = "Projects" )
@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
	
	Long projectId;
	
	String name;
	String description;
	Set<Meeting> meeting; //this is not on the db only for hibernate

	public Project() {
		
	}
	
	public Project(String projectName, String description) {
		this.name = projectName;
		this.description = description;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId(){
		return projectId;
	}
	
	private void setId(Long projectId){
		this.projectId = projectId;
	}
	
	@OneToMany(mappedBy="project")
	@Cascade({CascadeType.DELETE})
	public Set<Meeting> getMeeting() {
		return this.meeting;
	}
	
	public void setMeeting(Set<Meeting> meeting) {
		this.meeting = meeting;
	}
	
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String projectName) {
		this.name = projectName;
	}
	
	@Column(name="description")
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String description){
		this.description = description;
	}

}

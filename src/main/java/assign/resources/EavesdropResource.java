package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//import com.mysql.fabric.Response;
import javax.ws.rs.core.Response;

import assign.domain.Meeting;
import assign.domain.Project;
import assign.service.DBLoader;

@Path("/projects")
public class EavesdropResource {
	
	DBLoader dbLoader = new DBLoader();
	
	String password;
	String username;
	String dburl;	
	//String database;
	
	private Map<Long, Project> ProjectssDB = new ConcurrentHashMap<Long, Project>(); 
	private AtomicLong idCounter = new AtomicLong();
	
//	public EavesdropResource() {
//		this.dbLoader = new dbLoader();
//	}
	
	public EavesdropResource(@Context ServletContext servletContext) {
		//"jdbc:mysql://fall-2016.cs.utexas.edu:3306/cs378_Projectss"
		String dbhost = servletContext.getInitParameter("DBHOST");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		String database = servletContext.getInitParameter("DBNAME");
		dburl = "jdbc:mysql://" + dbhost +":3306/"+ database;
		//this.dbLoader = new dbLoaderImpl(dburl, username, password);		
	}
			
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {		
		return "Hello world " + dburl + " " + username + " " + password;		
	}

	//<project><projectId></projectsId><name>spiderman</name><description>hello</description></projects>
	@POST
	@Consumes("application/xml")
	public Response createProjects(Project project) throws Exception{
		
		Long projectId;
		
		try{
			project.setDescription("Project representing " + project.getName());
			projectId = dbLoader.addProject(project.getName(), project.getDescription());
			
		}catch(Exception e)
		{
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
		System.out.println("Created Projects " + projectId +" "+ project.getName());
		
		return Response.created(URI.create("/Projects/"+projectId)).build();
	}

	@POST
	@Path("/{projectId}/meetings")
	@Consumes("application/xml")
	public Response createMeeting(@PathParam("projectId") Long projectId, Meeting meeting) throws Exception{
		
		Long meetingId;
		Project projectResult = dbLoader.getProject(projectId);
		System.out.println("Here5");
		if (projectResult == null){
			System.out.println("Here6");
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		else{
		
			try{
				meetingId = dbLoader.addMeeting(meeting.getName(), meeting.getYear());
			}catch(Exception e)
			{
				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			}
			
			System.out.println("Created Meeting: " + meetingId +" Year: "+ meeting.getYear());
		}
		
		return Response.created(URI.create("/Projects/{projectId}/Meeting/"+meetingId)).build();
	
	}
	
	@GET
	@Path("/{projectId}")
	@Produces("application/xml")
	public StreamingOutput getProjects(@PathParam("projectId") Long projectId) throws Exception{
		
		final Project ProjectsResult = dbLoader.getProject(projectId);
		
		if (ProjectsResult == null){
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProject(outputStream, ProjectsResult);
	         }
	      };     
	}
	
	//curl -i --data "<Projects><ProjectsId>5</ProjectsId><name>assign.domain.Projects@52a15403</name><description>hello</description></Projects>" -H "Content-Type: application/xml" -X "PUT" http://localhost:8080/assignment4/myeavesdrop/Projectss/5	
	@PUT
	@Path("/{projectId}/meetings/{meetingId}")
	@Consumes("application/xml")
	public void updateProjects(@PathParam("projectId") Long projectId, @PathParam("meetingId") Long meetingId) throws Exception {

		Project current = dbLoader.getProject(projectId);

		if(current == null){
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		else{	
			
			try{
				Meeting meeting = dbLoader.getMeeting(meetingId);
				meeting.setName(meeting.getName() + "-name-changed");
				meeting.setYear(meeting.getYear() + "-description-changed");
				dbLoader.updateMeeting(meetingId);
				throw new WebApplicationException(Response.Status.OK);
				
			}catch(Exception e)
			{
				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			}
		}
	}


	//<project><projectId>1</projectId><name></name></project>
	@DELETE
	@Path("/{projectId}")
	public void delete(@PathParam("projectId") Long projectId) throws Exception{
		
		try{
			System.out.println("Resource Layer Delete: before GettingProject");
			Project delete =  dbLoader.getProject(projectId);
			
			if (delete == null){
				System.out.println("Resource Layer Delete: Project NotFound");
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
			
			else{
				System.out.println("Resource Layer Delete: About to delete the project");
				dbLoader.deleteProject(projectId);
				System.out.println("Resource Layer Delete: OK");
				throw new WebApplicationException(Response.Status.OK);
			}
		}catch(Exception e)
		{
			System.out.println("Resource Layer Delete: BadRequest");
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
	
	}
		
	protected void outputProject(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
}
	
